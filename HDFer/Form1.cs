﻿using System;
using System.Drawing;
using System.Windows.Forms;
using HDF5DotNet;
using System.IO;
using ZedGraph;

/* dolist
 1.При зактрытии удалять изображения
 2.четкость цв изображения
 3.Удалить дублирующие переменные lan lon и т.д
 4.Переименовать все элементы формы на интуитивно понятные
  */

namespace HDFER
{
    public partial class Form1 : Form
    {
        int X1, X2, Y1, Y2;
        float L1, L2;
        float LeftLon, RightLon, LeftLat, RightLat;
        int SCALE, OFFSET;
        int[,] readDataBack;
        Bitmap image = new Bitmap("NOFILE.jpg");
        Bitmap imagei;
        H5FileId file = null;
        private string[] files;
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Image = image;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void Form1Load(object sender, EventArgs e)
        {
            File.Delete("pic1.jpg");
            File.Delete("pic2.jpg");
            radioButtonGrey.Checked = true;
            radioButtonLatitude.Checked = true;
            groupBoxLatLon.Visible = false;
            zedGraphControl1.Visible = false;
        }

        private void openClick(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "HDF5 Files|*.hdf5|h5 files|*.h5";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                file = H5F.open(openDialog.FileName, H5F.OpenMode.ACC_RDONLY);
                MakeTree();
                //чтобы добраться непосредственно до данных, надо открыть Dataset,
                //в котором они лежат, ну и дальше. 

                var dataset = H5D.open(file, "/LEVEL3/NDVI/NDVI");
                var datagroup = H5G.open(file, "/LEVEL3/GEOMETRY");
                var dataspace = H5D.getSpace(dataset);
                var size = H5S.getSimpleExtentDims(dataspace);  //размерность 10080x110080, в моем случае
                readDataBack = new int[size[0], size[1]];
                H5D.read<int>(dataset, new H5DataTypeId(H5T.H5Type.NATIVE_INT),
                new H5Array<int>(readDataBack));
                //////////////////////////////////////////////////////получение scale и offset
                H5AttributeId scaleAttr = H5A.openName(dataset, "SCALE");
                float[] z = new float[1];
                H5A.read<float>(scaleAttr, new H5DataTypeId(H5T.H5Type.IEEE_F32LE), new H5Array<float>(z));
                SCALE = Convert.ToInt32(z[0]);
                H5A.close(scaleAttr);
                H5AttributeId OFFSETAtt = H5A.openName(dataset, "OFFSET");
                H5A.read<float>(OFFSETAtt, new H5DataTypeId(H5T.H5Type.IEEE_F32LE), new H5Array<float>(z));
                OFFSET = Convert.ToInt32(z[0]);
                ////////////////////////////////////////////////////////
                H5D.close(dataset);
                H5S.close(dataspace);
                H5F.close(file);
                string[] names_of_attr = {"BOTTOM_LEFT_LATITUDE", "BOTTOM_LEFT_LONGITUDE",
                "BOTTOM_RIGHT_LATITUDE", "BOTTOM_RIGHT_LONGITUDE",
                "TOP_LEFT_LATITUDE", "TOP_LEFT_LONGITUDE", "TOP_RIGHT_LATITUDE", "TOP_RIGHT_LONGITUDE"};
                /////////////////////////////////////////////////////////// получение данных геолокации снимка
                float[] dataLanLon = new float[8];
                float[] n = new float[1];
                for (int i = 0; i < 8; i++)
                {
                    H5AttributeId attributeId = H5A.openName(datagroup, names_of_attr[i]);
                    H5A.read<float>(attributeId, new H5DataTypeId(H5T.H5Type.IEEE_F32LE), new H5Array<float>(n));
                    dataLanLon[i] = n[0];
                    H5A.close(attributeId);
                    metaText.Text += names_of_attr[i] + ":  "+ n[0] + Environment.NewLine;
                    n[0] = 0;
                }
                H5G.close(datagroup);
                LeftLat = dataLanLon[0];
                RightLat = dataLanLon[4];
                LeftLon = dataLanLon[1];
                RightLon = dataLanLon[3];
                ///////////////////////////////////////////////////////////вычисление шагов
                L1 = (dataLanLon[3] - dataLanLon[1]) / size[0]; //градусов в 1 пикселе по долготе
                L2 = (dataLanLon[4] - dataLanLon[0]) / size[1]; // градусов в 1 пикселе по широте
                ///////////////////////////////////////////////////////////
                X1 = Convert.ToInt32((56 - dataLanLon[0]) / L2);
                X2 = Convert.ToInt32((57 - dataLanLon[0]) / L2);
                Y1 = Convert.ToInt32((84.5 - dataLanLon[1]) / L1);
                Y2 = Convert.ToInt32((85.5 - dataLanLon[1]) / L1);
                PictureBox1_load(Convert.ToInt32(size[1]) - X2, Convert.ToInt32(size[1]) - X1, Y1, Y2, readDataBack);
                treeView.ExpandAll();
            }
        }

        private void ExitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void InfoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Приложение расчитано на получение спутниковых данных из файлов формата HDF5 и проведению анализа по индексу NDVI"
                + Environment.NewLine + "Создатель: Григорьев Семен Александрович" + Environment.NewLine +
                "Научный руководитель: Катаев Михаил Юрьевич", "ИНФОРМАЦИЯ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void PictureBox1_load(int size1, int size11, int size2, int size22, int[,] readdatab)
        {
            int bm_width = size11 - size1, bm_height = size22 - size2;
            Bitmap bm = new Bitmap(bm_width, bm_height);
            for (int i = size1; i < size11; i++)
            {
                for (int j = size2; j < size22; j++)
                {
                    if (readdatab[i, j] == 255)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(0, 0, 0));
                    else if (readdatab[i, j] <= 20)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(4, 18, 60));
                    else if (readdatab[i, j] <= 25)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(196, 182, 166));
                    else if (readdatab[i, j] <= 28)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(191, 180, 116));
                    else if (readdatab[i, j] <= 36)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(191, 236, 89));
                    else if (readdatab[i, j] <= 44)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(180, 150, 108));
                    else if (readdatab[i, j] <= 52)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(164, 130, 76));
                    else if (readdatab[i, j] <= 59)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(148, 114, 60));
                    else if (readdatab[i, j] <= 67)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(127, 156, 46));
                    else if (readdatab[i, j] <= 79)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(148, 182, 20));
                    else if (readdatab[i, j] <= 91)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(116, 170, 4));
                    else if (readdatab[i, j] <= 103)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(100, 162, 4));
                    else if (readdatab[i, j] <= 115)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(68, 142, 4));
                    else if (readdatab[i, j] <= 127)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(93, 97, 4));
                    else if (readdatab[i, j] <= 139)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(28, 114, 4));
                    else if (readdatab[i, j] <= 162)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(4, 98, 4));
                    else if (readdatab[i, j] <= 185)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(2, 69, 0));
                    else if (readdatab[i, j] <= 208)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(6, 59, 7));
                    else if (readdatab[i, j] <= 231)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(2, 38, 2));
                    else if (readdatab[i, j] <= 254)
                        bm.SetPixel(i - size1, j - size2, Color.FromArgb(4, 18, 4));
                }
            }
            bm.RotateFlip(RotateFlipType.Rotate90FlipX);
            if (File.Exists("pic2.jpg"))
            {
                bm.Save("pic22.jpg");
                bm.Dispose();
                pictureBox1.Image.Dispose();
                imagei.Dispose();
                File.Delete("pic2.jpg");
                File.Move("pic22.jpg", "pic2.jpg");
            }
            else bm.Save("pic2.jpg");
            logText.Text += GetCurrentTimeForLog() + "Цветное изображение построено" + Environment.NewLine;
            bm = new Bitmap("pic2.jpg");
            bm.RotateFlip(RotateFlipType.Rotate270FlipX);
            for (int i = size1; i < size11; i++)
            {
                for (int j = size2; j < size22; j++)
                {
                    bm.SetPixel(i - size1, j - size2, Color.FromArgb(readdatab[i, j], readdatab[i, j], readdatab[i, j]));// СЕРЫЙ
                }
            }
            bm.RotateFlip(RotateFlipType.Rotate90FlipX);
            if (File.Exists("pic1.jpg"))
            {
                bm.Save("pic11.jpg");
                bm.Dispose();
                pictureBox1.Image.Dispose();
                imagei.Dispose();
                File.Delete("pic1.jpg");
                File.Move("pic11.jpg", "pic1.jpg");
            }
            else bm.Save("pic1.jpg");
            logText.Text += GetCurrentTimeForLog() + "Ч.Б. изображение построено" + Environment.NewLine;
            if (radioButtonGrey.Checked == true)
            {
                imagei = new Bitmap("pic1.jpg");
                pictureBox1.Image = imagei;
                logText.Text += GetCurrentTimeForLog() + "Ч.Б. изображение просматривается" + Environment.NewLine;
            }
            if (radioButton2.Checked == true)
            {
                imagei = new Bitmap("pic2.jpg");
                pictureBox1.Image = imagei;
                logText.Text += GetCurrentTimeForLog() + "Цветное изображение просматривается" + Environment.NewLine;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists("pic1.jpg"))
            {
                imagei = new Bitmap("pic1.jpg");
                pictureBox1.Image = imagei;
                logText.Text += GetCurrentTimeForLog() + "Ч.Б. изображение просматривается" + Environment.NewLine;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists("pic2.jpg"))
            {
                imagei = new Bitmap("pic2.jpg");
                pictureBox1.Image = imagei;
                logText.Text += GetCurrentTimeForLog() + "Цветное изображение просматривается" + Environment.NewLine;
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = String.Empty;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "jpg files (*.jpg)|*.jpg";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;
                if (radioButtonGrey.Checked == true)
                {
                    if (File.Exists("pic1.jpg"))
                    {
                        File.Copy("pic1.jpg", fileName);
                        logText.Text += GetCurrentTimeForLog() + "Ч.Б. изображение сохранено   " + fileName + Environment.NewLine;
                    }
                    else
                    {
                        MessageBox.Show("Файл отсутствует!", "Ошибка сохранения:", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        logText.Text += GetCurrentTimeForLog() + "Ошибка: Файл отсутствует!" + Environment.NewLine;
                    }
                }
                if (radioButton2.Checked == true)
                {
                    if (File.Exists("pic2.jpg"))
                    {
                        File.Copy("pic2.jpg", fileName);
                        logText.Text += GetCurrentTimeForLog() + "Цветное изображение сохранено  " + fileName + Environment.NewLine;
                    }
                    else
                    {
                        MessageBox.Show("Файл отсутствует!", "Ошибка сохранения:", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        logText.Text += GetCurrentTimeForLog() + "Ошибка: Файл отсутствует!" + Environment.NewLine;
                    }
                }
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxLatLon.Visible = false;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                logText.Text += GetCurrentTimeForLog() + "Доступ к директории:" + folderBrowserDialog1.SelectedPath + Environment.NewLine;
                files = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.HDF5");
                try
                {
                    for(int i = 0; i < files.Length; i++)
                    {
                        H5FileId file1 = H5F.open(files[i], H5F.OpenMode.ACC_RDONLY);

                        var dataset = H5D.open(file1, "/LEVEL3/NDVI/NDVI");
                        var datagroup = H5G.open(file1, "/LEVEL3/GEOMETRY");
                        var dataspace = H5D.getSpace(dataset);
                        var size = H5S.getSimpleExtentDims(dataspace);  //размерность 10080x110080, в моем случае
                        readDataBack = new int[size[0], size[1]];
                        H5D.read<int>(dataset, new H5DataTypeId(H5T.H5Type.NATIVE_INT),
                        new H5Array<int>(readDataBack));
                        //////////////////////////////////////////////////////получение scale и offset
                        H5AttributeId scaleAttr = H5A.openName(dataset, "SCALE");
                        float[] z = new float[1];
                        H5A.read<float>(scaleAttr, new H5DataTypeId(H5T.H5Type.IEEE_F32LE), new H5Array<float>(z));
                        SCALE = Convert.ToInt32(z[0]);
                        H5A.close(scaleAttr);
                        H5AttributeId OFFSETAtt = H5A.openName(dataset, "OFFSET");
                        H5A.read<float>(OFFSETAtt, new H5DataTypeId(H5T.H5Type.IEEE_F32LE), new H5Array<float>(z));
                        OFFSET = Convert.ToInt32(z[0]);
                        ////////////////////////////////////////////////////////
                        H5D.close(dataset);
                        H5S.close(dataspace);
                        H5F.close(file1);
                        string[] names_of_attr = {"BOTTOM_LEFT_LATITUDE", "BOTTOM_LEFT_LONGITUDE",
                "BOTTOM_RIGHT_LATITUDE", "BOTTOM_RIGHT_LONGITUDE",
                "TOP_LEFT_LATITUDE", "TOP_LEFT_LONGITUDE", "TOP_RIGHT_LATITUDE", "TOP_RIGHT_LONGITUDE"};
                        /////////////////////////////////////////////////////////// получение данных геолокации снимка
                        float[] dataLanLon = new float[8];
                        float[] n = new float[1];
                        for (int J = 0; J < 8; J++)
                        {
                            H5AttributeId attributeId = H5A.openName(datagroup, names_of_attr[J]);
                            H5A.read<float>(attributeId, new H5DataTypeId(H5T.H5Type.IEEE_F32LE), new H5Array<float>(n));
                            dataLanLon[J] = n[0];
                            H5A.close(attributeId);
                            metaText.Text += names_of_attr[J] + ":  " + n[0] + Environment.NewLine;
                            n[0] = 0;
                        }
                        H5G.close(datagroup);
                        LeftLat = dataLanLon[0];
                        RightLat = dataLanLon[4];
                        LeftLon = dataLanLon[1];
                        RightLon = dataLanLon[3];
                        ///////////////////////////////////////////////////////////вычисление шагов
                        L1 = (dataLanLon[3] - dataLanLon[1]) / size[0]; //градусов в 1 пикселе по долготе
                        L2 = (dataLanLon[4] - dataLanLon[0]) / size[1]; // градусов в 1 пикселе по широте
                                                                        ///////////////////////////////////////////////////////////
                        X1 = Convert.ToInt32((56 - dataLanLon[0]) / L2);
                        X2 = Convert.ToInt32((57 - dataLanLon[0]) / L2);
                        Y1 = Convert.ToInt32((84.5 - dataLanLon[1]) / L1);
                        Y2 = Convert.ToInt32((85.5 - dataLanLon[1]) / L1);
                    }
                    zedGraphControl1.Visible = true;
                    CreateGraph3(zedGraphControl1);
                }
                catch
                {
                    MessageBox.Show("Неравильно выбран файл(ы)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (files.Length == 0)
                {
                    MessageBox.Show("В папке нет файлов", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxLatLon.Visible = true;
            zedGraphControl1.Visible = true;
            if (File.Exists("pic1.jpg") || File.Exists("pic2.jpg"))
            {
            }
            else
            {
                openClick(sender, e);
            }
            logText.Text += GetCurrentTimeForLog() + "Граф Y(X)" + Environment.NewLine;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxLatLon.Visible = false;
            logText.Text += GetCurrentTimeForLog() + "Граф области" + Environment.NewLine;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                MessageBox.Show("Введите широту " + LeftLat + " до " + RightLat + "или долготу в диапозоне от " + LeftLon + " до " + RightLon, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                if (radioButtonLatitude.Checked)//широта
                {
                    if ((Convert.ToDouble(textBox2.Text) > LeftLat) && (Convert.ToDouble(textBox2.Text) < RightLat))
                    {
                        CreateGraph(zedGraphControl1);
                    }
                    else
                    {
                        MessageBox.Show("Введенные параметры не соответствуют рассматриваемой области. Параметр должен находиться в интервале от " + LeftLat + " до " + RightLat, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                if (radioButton7.Checked)//долгота
                {
                    if ((Convert.ToDouble(textBox2.Text) > LeftLon) && (Convert.ToDouble(textBox2.Text) < RightLon))
                    {
                        CreateGraph2(zedGraphControl1);
                    }
                    else
                    {
                        MessageBox.Show("Введенные параметры не соответствуют рассматриваемой области. Параметр должен находиться в интервале от " + LeftLon + " до " + RightLon, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }
        private Color color = Color.FromArgb(0, 0, 255);

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string path = treeView.SelectedNode.FullPath;
            string parentPath = treeView.SelectedNode.Parent.FullPath;
            parentPath = parentPath == "ROOT" ? "/" : parentPath.Remove(0, 4).Replace("\\", "/");
            //H5GroupId group1 = H5G.open(file, parentPath);
            path = path == "ROOT" ? "/" : path.Remove(0, 4).Replace("\\", "/");
            //if (H5G.getObjectInfo(group, path, true).objectType.ToString() == "GROUP")
            //{
            //    H5GroupId groupIn = H5G.open(file, path);
            //    metaText.Text += "OK";
            //}
            //metaText.Text += path + Environment.NewLine + parentPath + Environment.NewLine;
        }

        private void CreateGraph(ZedGraphControl zgczedGraphControl1)
        {
            // Получим панель для рисования
            GraphPane g = zedGraphControl1.GraphPane;
            g.Title.Text = "NDVI по широте";
            g.XAxis.Title.Text = "NDVI";
            g.YAxis.Title.Text = "Y";
            g.CurveList.Clear();
            // Спискок точек
            int XX = Convert.ToInt32((Convert.ToDouble(textBox2.Text) - 56) / L2);//заменить
            PointPairList list1 = new PointPairList();
            for (int j = Y1; j < Y2; j++)
            {
                list1.Add(j - Y1, readDataBack[10080 - XX, j]);//заменить
            }
            // Создадим кривую
            LineItem myCurve1 = g.AddCurve("Y(x)", list1, color, SymbolType.Default);
            // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
            // В противном случае на рисунке будет показана только часть графика, 
            // которая умещается в интервалы по осям, установленные по умолчанию
            zedGraphControl1.AxisChange();
            // Обновляем график
            zedGraphControl1.Refresh();
        }

        private void CreateGraph2(ZedGraphControl zgczedGraphControl1)
        {
            // Получим панель для рисования
            GraphPane g = zedGraphControl1.GraphPane;
            g.Title.Text = "NDVI по долготе";
            g.XAxis.Title.Text = "NDVI";
            g.YAxis.Title.Text = "Y";
            g.CurveList.Clear();
            // Спискок точек
            int YY = Convert.ToInt32((Convert.ToDouble(textBox2.Text) - 84.5) / L1);//заменить
            PointPairList list1 = new PointPairList();
            for (int i = X1; i < X2; i++)
            {
                list1.Add(i - X1, readDataBack[i, YY]);
            }
            // Создадим кривую
            LineItem myCurve1 = g.AddCurve("Y(x)", list1, color, SymbolType.Default);
            // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
            // В противном случае на рисунке будет показана только часть графика, 
            // которая умещается в интервалы по осям, установленные по умолчанию
            zedGraphControl1.AxisChange();
            // Обновляем график
            zedGraphControl1.Refresh();
        }
        private void CreateGraph3(ZedGraphControl zgczedGraphControl1)
        {
            GraphPane g = zedGraphControl1.GraphPane;
            g.Title.Text = "NDVI";
            g.XAxis.Title.Text = "t";
            g.YAxis.Title.Text = "NDVI";
            g.CurveList.Clear();
            int mY = 40;
            PointPairList list1 = new PointPairList();
            for (int i = X1; i < X2; i++)
            {
                list1.Add(i - X1, readDataBack[i, mY]);
                //if (list1.ToArray(). > 40)
            }
            LineItem myCurve1 = g.AddCurve("Y(x)", list1, color, SymbolType.Default);
            zedGraphControl1.AxisChange();
            // Обновляем график
            zedGraphControl1.Refresh();
        }

        private string GetCurrentTimeForLog()
        {
            return DateTime.Now.ToString() + "  ";
        }

        private void MakeTree()
        {
            H5GroupId root = H5G.open(file, "/");
            TreeNode ROOT = new TreeNode("ROOT");
            treeView.Nodes.Add(ROOT);
            string puth = "/";
            logText.Text += GetCurrentTimeForLog() + "Root-Gruppe:" + Environment.NewLine;
            AddNodesInTree(ROOT, puth, root);
            H5G.close(root);
        }
        private void AddNodesInTree(TreeNode parent, string path, H5GroupId groupId)
        {
            if (H5G.getObjectInfo(groupId, path, true).objectType.ToString() == "GROUP")
            {
                H5GroupId group = H5G.open(file, path);
                long elementeGroup = H5G.getNumObjects(group);
                if (elementeGroup > 0)
                {
                    for (int i = 0; i < elementeGroup; i++)
                    {
                        string obj_name = H5L.getNameByIndex(file, path, H5IndexType.NAME, H5IterationOrder.NATIVE, i);
                        TreeNode node = new TreeNode(obj_name);
                        parent.Nodes.Add(node);
                        logText.Text += "       " + obj_name + " - " + H5G.getObjectInfo(groupId, path, true).objectType.ToString() + Environment.NewLine;
                        AddNodesInTree(node, path == "/" ? path + obj_name : path + "/" + obj_name, group);
                    }
                }
                H5G.close(group);
            } else
            {
                logText.Text += "       " + parent.Text + " - " + H5G.getObjectInfo(groupId, path, true).objectType.ToString() + Environment.NewLine;
            }
        }
    }
}
